package org.pom.practice.Amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.pages.amazon.LoginPage;
import com.pages.amazon.Register_tab;

public class Test1 extends Webelement_Actions {

	@Test(priority=1)
	public void verify_login() {

		WebDriver signin = Browsers.startbrowser("chrome", "https://gitlab.com/users/sign_in");

		LoginPage log = PageFactory.initElements(signin, LoginPage.class);

		log.logindetails("admin123", "qwerty123mnb");

	}

	@Test
	public void verify_registeration() {

		WebDriver signup = Browsers.startbrowser("firefox", "https://gitlab.com/users/sign_in");

		Register_tab signup_1 = PageFactory.initElements(signup, Register_tab.class);

		signup_1.register_actions("admin", "rock", "adminrock123@gmail.com", "adminrock123@gmail.com", "mnbvcxz");
	}

}
