package org.pom.practice.Amazon;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Browsers {

	public static WebDriver driver;

	public static WebDriver startbrowser(String browsername, String Url) {

		if (browsername.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver", "C:/Users/ramchandhn/fpr/practice/driver/chromedriver.exe");

			driver = new ChromeDriver();

		}

		else if (browsername.equalsIgnoreCase("firefox")) {

			System.setProperty("webdriver.gecko.driver", "C:/Users/ramchandhn/fpr/practice/driver/geckodriver.exe");

			driver = new FirefoxDriver();

		}

		driver.manage().window().maximize();

		driver.get(Url);

		return driver;

	}

}
